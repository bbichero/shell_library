# Define color
YEL='\033[1;33m'
GRE='\033[0;32m'
RED='\033[0;31m'
NC='\033[0m' # No Color
DEBUG=0

# Print a loading bar
# to use it :
# <long command to process ... > &
# ft_spinner
function	ft_spinner()
{
	local pid=$!
	local delay=0.75
	local spinstr='...'

	while [ "$(ps a | awk '{print $1}' | grep $pid)" ]
	do
		local temp=${spinstr#?}
		printf "%s " "$spinstr"
		local spinstr=$temp${spinstr%"$temp"}
		sleep $delay
		printf "\b\b\b"
	done
	echo
}

# Take one argument, a directory name
function	ft_exist_dir()
{
	# If DEBUG>1 print info
  	if [ $DEBUG -gt 1 ]
	then
		echo "Enter in ft_exist_dir() with value '`echo $1`'"
	fi
  
	# If directory exist print error
	if [ -d "$1" ]
	then
    		echo -e "${RED}Error: Directory '`echo "$1"`' already exists.${NC}"
		return 0
	else
 		return 1
	fi
}

# Check if package in installed
function	ft_installed_package()
{
	# If $DEBUG>1 print info
	if [ $DEBUG -gt 1 ]
	then
		echo "Enter in ft_installed_package() with value '`echo $1`'"
	fi

	# Check if package is install with command dpkg
	if [ "$(dpkg -s ${1} 2>&1 | grep 'install ok installed')" == "" ]
	then
		echo -e ${RED}"Package '`echo $1`' not installed, try to install it"${NC}

		# Check if sudo pckage is installed
		if [ "$(dpkg -s 'sudo' 2>&1 | grep 'install ok installed')" == "" ]
		then
			# Check if user is root, else you can't install package
			if [ "$EUID" -ne 0 ]
				then echo "Can't install package, you are not root and sudo ins't installed"
				exit 1
			else
				echo -e ${YEL}"apt-get install $1"${NC}
				apt-get install "$1" > /dev/null &
				ft_spinner
			fi
		else
			# Install package with sudo
			echo -e ${YEL}"sudo apt-get install $1"${NC}
			sudo apt-get install "$1" > /dev/null &
			ft_spinner
		fi
	fi
}

# Call when CTRL C is catch
function 	gracefulExit()
{
	# Unbug shell
	stty sane
	exit 1
}
