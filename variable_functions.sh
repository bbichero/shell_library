# Define color
YEL='\033[1;33m'
GRE='\033[0;32m'
RED='\033[0;31m'
NC='\033[0m' # No Color
DEBUG=0

# Get back index of variable in array
function	ft_get_index()
{
	# Can't debug here, because output is use as return function
 
	# Redeclare array from 2nd argument
	local array=("${@:2}")
 
	for i in "${!array[@]}"
	do
		if [ "${array[$i]}" == "$1" ]
		then
			# Return index value of first argument
			echo "$i"
 
			# Unset variable
			unset array
			return 0
		fi
	done
}

# Check if array contain a certain element
function	ft_array_contains()
{
	# Redeclare array to print it in debug
	local array=("${@:2}")

	# If DEBUG>1 print info
	if [ $DEBUG -gt 1 ]
	then
		echo -e ${YEL}"Enter in array_contains() with value '`echo $1`', and '`echo ${array[@]}`' value"${NC}
	fi
 
	for i in ${!array[@]}
	do
		# IF DEBUG>1 print each line comparison
		if [ $DEBUG -gt 1 ]
		then
			echo "Element of array   : '`echo ${array[$i]}`'"
			echo "Element in argument: '`echo $1`'"
		fi
		# If First argument match with the element in array, echo 1 and exit function
		if [ "${array[$i]}" == "$1" ]
		then
			# If debug=1 print
			if [ $DEBUG -gt 0 ]
			then
				echo -e ${GRE}"Match line in array: '`echo ${array[$i]}`'"${NC}
			fi
			return 1
		fi
	done
	return 0
 
	# Unset variable
	unset i array
}

# Take a custom valid array {"val1", "val2"}, no mandatory quote or comma
function	ft_get_value_arr()
{
	# Can't debug here because all echo are used for return

	# Check validity of array
	array_pattern="{([\'\"]?[^\s\'\"]*[\'\"]?,?(, )?)+}"

	# Apply regex
	array_match=$(echo "$1" | grep -Po "$array_pattern")

	# Remove '{' and '}' from string
	array=$(echo "$1" | rev | cut -c2- | rev | cut -c2-)

	# If regex return nothing exit with error code 1
	if [ ${#array_match} -eq 0 ]
	then
		exit 1
	fi

	# Split line with ',' delimitor
	IFS=',' read -r -a new_array <<< "$array"

	# Trim each element of aray
	for i in "${!new_array[@]}"
	do
		new_array[$i]=$(echo "${new_array[$i]}" | sed -e 's/^[ \t]*//')
#		echo ${new_array[$i]}
	done

	# Print whole array
	echo "${new_array[@]}"

	# Unset variable
	unset array_pattern array_match

	return 0
}
