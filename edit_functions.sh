# Define color
# List of color : https://unix.stackexchange.com/questions/124407/what-color-codes-can-i-use-in-my-ps1-prompt
YEL='\033[1;33m'   # Yellow
ORA='\033[1;214m'  # Orange
GRE='\033[0;32m'   # Green
RED='\033[0;31m'   # Red
NC='\033[0m' 	   # No Color
DEBUG=0

# Take 3 arguments
# First => line to edit
# Second => replacement line
# Third => filename
# Return : 0 if editing done, 2 if file not found,
# 3 if line given ins't in directory and 1 if other error
function	ft_replace_line()
{
	# If DEBUG>1 print infos
	if [ $DEBUG -gt 1 ]
	then
		echo -e ${YEL}"Enter in ft_edit_line_file() with string '`echo $1`'"${NC}
	fi

	# Check if first argument isn't empty
	if [ ${#1} -eq 0 ]
	then
		echo -e ${RED}"First argument is empty, can't process, abort"${NC}
		return 1
	fi
	
	# Check if file exist
	if [ ! -f "$3" ]
	then
		echo -e ${RED}"Can't find file: '`echo $3`', abort"${NC}
		return 2
	fi

	# Find line in file
	if [ "$(grep "$1" "$3")" == "" ]
	then
		echo -e ${RED}"Can't find line '`echo $1`' in file, abort"${NC}
		return 3
	fi

	# Replace line with sed
	sed -i.bak "s/$1/$2/g" "$3"

	return 0
}
