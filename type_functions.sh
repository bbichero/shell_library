# Define color
YEL='\033[1;33m'
GRE='\033[0;32m'
RED='\033[0;31m'
NC='\033[0m' # No Color
DEBUG=0

# take one arg, and ipv4, return 1 if valid or 0 if not
function	ft_is_ipv4()
{
	# If DEBUG>1 print info
	if [ $DEBUG -gt 1 ]
	then
		echo "Enter in ft_is_ipv4() with '`echo $1`' value"
	fi

	# IPv6 check
	#if [[ $s =~ ^(([0-9a-fA-F]{1,4}:){7,7}[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,7}:|([0-9a-fA-F]{1,4}:){1,6}:[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,5}(:[0-9a-fA-F]{1,4}){1,2}|([0-9a-fA-F]{1,4}:){1,4}(:[0-9a-fA-F]{1,4}){1,3}|([0-9a-fA-F]{1,4}:){1,3}(:[0-9a-fA-F]{1,4}){1,4}|([0-9a-fA-F]{1,4}:){1,2}(:[0-9a-fA-F]{1,4}){1,5}|[0-9a-fA-F]{1,4}:((:[0-9a-fA-F]{1,4}){1,6})|:((:[0-9a-fA-F]{1,4}){1,7}|:)|fe80:(:[0-9a-fA-F]{0,4}){0,4}%[0-9a-zA-Z]{1,}|::(ffff(:0{1,4}){0,1}:){0,1}((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])|([0-9a-fA-F]{1,4}:){1,4}:((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9]))$ ]]; then
	if [[ $1 =~ ^([0-9]{1,2}|1[0-9][0-9]|2[0-4][0-9]|25[0-5])\.([0-9]{1,2}|1[0-9][0-9]|2[0-4][0-9]|25[0-5])\.([0-9]{1,2}|1[0-9][0-9]|2[0-4][0-9]|25[0-5])\.([0-9]{1,2}|1[0-9][0-9]|2[0-4][0-9]|25[0-5])$ ]]
	then
  		return 0
	else
		echo -e ${RED}"Value '`echo $1`' isn't a valid IPv4 address"${NC}
		return 1
	fi
}

# Take one arg, and int, return 1 if variable is an INT or 0 if not
function	ft_is_int()
{
	# If DEBUG>1 print info
	if [ $DEBUG -gt 1 ]
	then
		echo "Enter in is_int() with '`echo $1`' value"
	fi

	# Check if value is numeric
	if [ "$1" -eq "$1" ] 2>/dev/null
	then
		return 0
	else
		echo -e ${RED}"Value '`echo $1`' isn't numeric"${NC}
		return 1
	fi
}

# Check if arg is an array, returns 1 on sucess, 0 otherwise
function	ft_is_array()
{
	# If DEBUG>1 print info
	if [ $DEBUG -gt 1 ]
	then
		echo "Enter in is_array() with '`echo $1`' value"
	fi

	[ -z "$1" ] && return 1
	if [ -n "$BASH" ]
	then
		declare -p ${1} 2> /dev/null | grep 'declare \-a' >/dev/null && return 0
	fi
	echo -e ${RED}"Value '`echo $1`' isn't an array"${NC}
	return 1
}

# Check if arg is a valid MAC address, returns 1 on sucess, 0 otherwise
function ft_is_mac_addr()
{
	# If DEBUG>1 print info
	if [ $DEBUG -gt 1 ]
	then
		echo "Enter in is_mac_addr() with '`echo $1`' value"
	fi

	# Check if argument is MAC address
	if [[ "$1" =~ ^([a-fA-F0-9]{2}:){5}[a-fA-F0-9]{2}$ ]]
	then
		return 0
	else
		echo -e "${RED}Error: Bad mac address given.${NC}"
		return 1
	fi
}
